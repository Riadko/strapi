### Prérequis

1. **Installer Node.js et npm** :
   - Assurez-vous que Node.js (version 12.x ou plus récente) et npm sont installés sur votre machine.
   - Vous pouvez vérifier les versions installées en exécutant `node -v` et `npm -v` dans votre terminal.

2. **Installer PostgreSQL** :
   - Téléchargez et installez PostgreSQL à partir de [la page officielle de PostgreSQL](https://www.postgresql.org/download/).
   - Durant l'installation, notez le mot de passe pour l'utilisateur `postgres`, car il sera nécessaire pour accéder à votre base de données.

3. **Configurer PostgreSQL** :
   - Après l'installation, ouvrez PostgreSQL (via pgAdmin ou la ligne de commande) et créez une nouvelle base de données pour Strapi.
   - Notez le nom de la base de données, l'utilisateur (généralement `postgres`), et le mot de passe.

### Étape 1 : Installation de Strapi

1. **Créer un Nouveau Projet Strapi** :
   - Ouvrez votre terminal.
   - Exécutez la commande suivante pour créer un nouveau projet Strapi sans démarrage rapide (qui installe SQLite par défaut) :
     ```
     npx create-strapi-app mon-projet
     ```
   - `mon-projet` est le nom que vous souhaitez donner à votre projet Strapi.

2. **Configurer Strapi avec PostgreSQL** :
   - Lorsque vous êtes invité à choisir le type de base de données, sélectionnez `PostgreSQL`.
   - Remplissez les détails de connexion à la base de données avec les informations que vous avez notées précédemment (nom de la base de données, utilisateur, mot de passe, etc.).

### Étape 2 : Configuration du Projet
2. **Démarrer Strapi** : Une fois Strapi installé, démarrez le serveur Strapi. En général, vous le faites en naviguant dans le dossier de votre projet Strapi et en exécutant `npm run develop`.
3. **Accéder au Panneau d'Administration** : Ouvrez votre navigateur et allez sur `http://localhost:1337/admin` pour accéder au panneau d'administration de Strapi.
4. **Créer un Compte Administrateur** : Suivez les instructions à l'écran pour créer un compte administrateur.

### Étape 3 : Création de Types de Contenu

#### 3.1 Créer un Type de Contenu pour les Articles de Blog
1. **Accéder au Constructeur de Types de Contenu** :
   - Dans le panneau d'administration de Strapi, cliquez sur "Content-Types Builder" dans le menu de gauche.

2. **Créer un Nouveau Type de Collection** :
   - Cliquez sur le bouton “Create new collection type”.

3. **Nommer le Type de Collection** :
   - Saisissez le nom pour votre type de collection, par exemple, `Article`. Cliquez sur `Continue`.

4. **Ajouter des Champs au Type de Collection** :
   - Cliquez sur “Add another field” et choisissez les types de champs appropriés pour votre blog. Voici quelques champs courants pour un article de blog :
     - **Title** : Choisir 'Text' comme type de champ pour le titre de l'article.
     - **Body** : Sélectionnez 'Rich Text' pour le corps de l'article.
    - Cliquez sur `Finish` pour chaque champ, puis sur `Save` en haut à droite une fois tous les champs ajoutés.

### Étape 4 : Configuration des Permissions

#### 4.1 Accéder aux Paramètres de Permissions
1. **Ouvrir les Paramètres de Strapi** :
   - Dans le panneau d'administration de Strapi, cliquez sur “Settings” situé dans le menu latéral en bas à gauche.

2. **Naviguer vers les Utilisateurs et Permissions** :
   - Dans la section “Settings”, recherchez et cliquez sur “Users & Permissions plugin”. Cela ouvre la page de configuration des rôles et permissions.

#### 4.2 Configurer les Permissions pour le Rôle Public
3. **Sélectionner le Rôle Public** :
   - Dans l'onglet “Roles”, vous verrez une liste de rôles. Cliquez sur le rôle “Public” pour configurer les permissions accessibles à tous les utilisateurs non authentifiés.

4. **Configurer les Permissions pour les Articles et Catégories** :
   - Dans la page de configuration du rôle “Public”, faites défiler jusqu'à la section où vos types de contenu (par exemple, “Articles”) sont listés.
   - Pour chaque type de contenu (comme “Articles”), configurez les permissions en cochant les toutes les cases.

#### 4.3 Sauvegarder les Modifications
5. **Enregistrer les Paramètres de Permissions** :
   - Après avoir coché les cases appropriées, cliquez sur le bouton “Save” en haut à droite de la page pour enregistrer vos modifications.

### Installer et configuer Prometheus 
Pour surveiller notre application Strapi on peut utiliser Prometheus.
Prometheus est un système de surveillance et d'alerte open source qui collecte des métriques à partir de cibles configurées à des intervalles définis. 

1. **Installer Prometheus** :
   - Vous devez d'abord installer Prometheus sur votre serveur ou machine locale. Vous pouvez trouver les instructions d'installation sur le [site officiel de Prometheus](https://prometheus.io/download/).

2. **Configurer Prometheus pour surveiller Strapi** :
   - Après avoir installé Prometheus, vous devez le configurer pour qu'il puisse gratter (scrape) les métriques de votre application Strapi.
   - Ouvrez le fichier de configuration de Prometheus (généralement nommé `prometheus.yml`) et ajoutez votre application Strapi en tant que cible.
   - Par exemple :
     ```yaml
      global:
          scrape_interval: 15s # Par défaut, récupère les cibles toutes les 15 secondes.

      scrape_configs:
      - job_name: 'projetStrapi'
          static_configs:
          - targets: ['strapi:1337']
          metrics_path: '/api/metrics/'  # Spécifie le chemin des métriques pour les articles de l'API
     ```
   - Ici, `strapi:1337` devrait être remplacé par l'adresse et le port de votre application Strapi.

**Vérifier les métriques** :
   - Accédez à l'interface utilisateur de Prometheus, généralement disponible à `http://localhost:9090`, et utilisez son explorateur pour voir si les métriques de Strapi sont collectées.

**ajout du plugin Prometheus** :
    - Dans le dossier config de votre projet Strapi, ajouté le fichier plugin.js :
```js
const { apolloPrometheusPlugin } = require('strapi-prometheus');

// Créez l'objet de configuration du plugin en dehors de la déclaration de l'objet
const prometheusPluginConfig = {
  enabled: true,
};

module.exports = {
  plugins: [
    {
      plugin: require('strapi-prometheus'),
      config: prometheusPluginConfig,
    },
  ],
  graphql: {
    enabled: true,
    config: {
      apolloServer: {
        plugins: [apolloPrometheusPlugin], // ajoutez le plugin pour obtenir des métriques Apollo
        tracing: true, // cela doit être true pour obtenir certaines des données nécessaires à la création des métriques
      },
    },
  },
};
```

- installé aussi le module strapi-prometheus avec : **node install strapi-prometheus**
- Rebuild votre projet Strapi, et après reconnecté vous et dans Configuration des Permissions refaite les memes étapes et faites défiler jusqu'à Prometheus et configurez les permissions en cochant toutes les cases.

**Utiliser Grafana pour la visualisation** :
   L'installation et la configuration de Grafana pour travailler avec Prometheus se fait en plusieurs étapes. Grafana est un outil puissant pour créer des tableaux de bord et visualiser des données, notamment des métriques collectées par Prometheus. Voici comment procéder :

	1. **Téléchargement et Installation** :
   - Sur un serveur Linux, vous pouvez installer Grafana en utilisant le gestionnaire de paquets approprié. Par exemple, sur un système basé sur Debian/Ubuntu, utilisez :
     ```bash
     sudo apt-get install -y software-properties-common
     sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
     wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
     sudo apt-get update
     sudo apt-get install grafana
     ```
   - Pour d'autres systèmes d'exploitation, suivez les instructions sur le [site officiel de Grafana](https://grafana.com/grafana/download).

	1. **Démarrage du Service Grafana** :
   - Une fois installé, démarrez le service Grafana et assurez-vous qu'il s'exécute automatiquement au démarrage :
     ```bash
     sudo systemctl start grafana-server
     sudo systemctl enable grafana-server.service
     ```

	3. **Accéder à l'Interface Web de Grafana** :
   - Ouvrez un navigateur et accédez à `http://<your-server-ip>:3000`. L'adresse IP doit être celle du serveur où Grafana est installé. Par défaut, Grafana utilise le port `3000`.
   - Les identifiants par défaut sont `admin` pour l'utilisateur et le mot de passe.

	4. **Changer le Mot de Passe par Défaut** :
   - Lors de la première connexion, Grafana vous invite à changer le mot de passe de l'administrateur. Il est fortement recommandé de le faire pour des raisons de sécurité.

	5. **Ajouter Prometheus en tant que Source de Données** :
   - Allez dans les paramètres (l'icône de rouage sur le côté gauche) et sélectionnez "Data Sources".
   - Cliquez sur "Add Data Source" et choisissez Prometheus.
   - Dans les paramètres de la source de données, entrez l'URL de votre serveur Prometheus (par exemple `http://localhost:9090` si Prometheus s'exécute sur le même serveur que Grafana).
   - Cliquez sur "Save & Test" pour vous assurer que Grafana peut se connecter à Prometheus.

	6. **Créer des Tableaux de Bord** :
   - Maintenant, vous pouvez créer des tableaux de bord dans Grafana pour visualiser les données de Prometheus.
   - Allez à "Dashboard", puis "New Dashboard" et commencez à ajouter des panneaux (graphs, tableaux, etc.) en utilisant les métriques disponibles depuis Prometheus.

### Dockerfile et docker-composer
- créez deux fichiers :
  
Dockerfile :

```yaml
FROM node:18-alpine

# Installing libvips-dev for sharp Compatibility
RUN apk update && apk add --no-cache build-base gcc autoconf automake zlib-dev libpng-dev nasm bash vips-dev git

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

WORKDIR /opt/
COPY package.json package-lock.json ./
RUN npm install -g node-gyp
RUN npm install -g esbuild-wasm
RUN npm config set fetch-retry-maxtimeout 600000 -g
RUN npm cache clean --force
RUN npm cache clean --force --cache /opt/.npm
RUN npm ci --legacy-peer-deps

WORKDIR /opt/app
COPY . .


EXPOSE 1337
CMD ["npm", "run", "develop"]
# CMD ["yarn", "develop"]
```
et docker-composer.yml :

```yaml
version: "3"
services:
strapi:
    container_name: strapi
    build: .
    image: strapi:latest
    restart: unless-stopped
    env_file: .env
    environment:
    DATABASE_CLIENT: ${DATABASE_CLIENT}
    DATABASE_HOST: strapiDB
    DATABASE_PORT: ${DATABASE_PORT}
    DATABASE_NAME: ${DATABASE_NAME}
    DATABASE_USERNAME: ${DATABASE_USERNAME}
    DATABASE_PASSWORD: ${DATABASE_PASSWORD}
    JWT_SECRET: ${JWT_SECRET}
    ADMIN_JWT_SECRET: ${ADMIN_JWT_SECRET}
    APP_KEYS: ${APP_KEYS}
    NODE_ENV: ${NODE_ENV}
    volumes:
    - ./config:/opt/app/config
    - ./src:/opt/app/src
    - ./package.json:/opt/package.json
    - ./yarn.lock:/opt/yarn.lock
    - ./.env:/opt/app/.env
    - ./public/uploads:/opt/app/public/uploads
    ports:
    - "1337:1337"
    networks:
    - strapi
    depends_on:
    - strapiDB
    deploy:
    resources:
        limits:
        cpus: '2'
        memory: '1G'

strapiDB:
    container_name: strapiDB
    platform: linux/amd64 #for platform error on Apple M1 chips
    restart: unless-stopped
    env_file: .env
    image: postgres:12.0-alpine
    environment:
    POSTGRES_USER: ${DATABASE_USERNAME}
    POSTGRES_PASSWORD: ${DATABASE_PASSWORD}
    POSTGRES_DB: ${DATABASE_NAME}
    volumes:
    - strapi-data:/var/lib/postgresql/data/ #using a volume
    #- ./data:/var/lib/postgresql/data/ # if you want to use a bind folder
    ports:
    - "5433:5432"
    networks:
    - strapi
prometheus:
    image: prom/prometheus
    volumes:
        - ./prometheus-2.49.1.windows-amd64/:/etc/prometheus/
    command:
        - "--config.file=/etc/prometheus/prometheus.yml"
    ports:
        - '9090:9090'
    networks:
        - strapi
    depends_on:
        - strapi
grafana:
    image: grafana/grafana
    environment:
    - GF_SECURITY_ADMIN_PASSWORD=admin
    ports:
    - '3001:3000'
    depends_on:
    - prometheus
    networks:
    - strapi

volumes:
strapi-data:
networks:
strapi:
    name: Strapi
    driver: bridge
```
- faire tourner votre Docker avec la commande : docker compose up -d --build

**Requetes et appels d'API**
- Créez un fichier javaScript :
```js
const axios = require('axios');
const readline = require('readline');

// Configuration
const API_URL = process.env.API_URL || "http://localhost:1337/api/articles";
let TOTAL_REQUESTS = process.env.TOTAL_REQUESTS || 100;

// Create readline interface
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Function to prompt user for total requests
function promptTotalRequests() {
    return new Promise((resolve) => {
      rl.question('Enter the total number of requests to send: ', (input) => {
        TOTAL_REQUESTS = parseInt(input) || TOTAL_REQUESTS;
        resolve();
      });
    });
}

// Fonction pour créer une ressource
async function createRequest() {
  try {
    const response = await axios.post(API_URL, {
      data: { Title: 'New Article', Body: 'Lorem Ipsum' }
    });

    console.log('Create Request Response:', response.data);
  } catch (error) {
    console.error('Error in create request:', error.message);
  }
}

// Fonction pour lire une ressource
async function readRequest() {
  try {
    const response = await axios.get(API_URL);

    console.log('Read Request Response:', response.data);
  } catch (error) {
    console.error('Error in read request:', error.message);
  }
}

// Fonction pour mettre à jour une ressource
async function updateRequest() {
  try {
    const articles = await axios.get(API_URL);
    const articleToUpdate = articles.data.data[0];

    if (articleToUpdate) {
      // Adaptation de la structure des données pour la mise à jour
      const response = await axios.put(`${API_URL}/${articleToUpdate.id}`, {
        data: { Title: 'Updated Article', Body: 'New Content' }
      });

      console.log('Update Request Response:', response.data);
    } else {
      console.error('No articles found for update');
    }
  } catch (error) {
    console.error('Error in update request:', error.message);
  }
}

// Fonction pour supprimer une ressource
async function deleteRequest() {
  try {
    const articles = await axios.get(API_URL);
    const articleToDelete = articles.data.data[0];

    if (articleToDelete) {
      const response = await axios.delete(`${API_URL}/${articleToDelete.id}`);

      console.log('Delete Request Response:', response.data);
    } else {
      console.error('No articles found for delete');
    }
  } catch (error) {
    console.error('Error in delete request:', error.message);
  }
}

// Boucle pour envoyer les requêtes en alternance
async function simulateLoad() {
  for (let i = 1; i <= Number(TOTAL_REQUESTS); i++) {
    switch (i % 4) {
      case 0:
        await createRequest();
        break;
      case 1:
        await readRequest();
        break;
      case 2:
        await updateRequest();
        break;
      case 3:
        await deleteRequest();
        break;
    }

    console.log(`Request ${i} sent`);
  }
  console.log('Load simulation completed');
  rl.close();
}

// Appel de la fonction principale
(async () => {
    await promptTotalRequests(); // Prompt user for total requests
    simulateLoad(); // Start load simulation
})();
```
- installer axios avec : **npm install axios**
- Lancez avec le code avec la commande : **node votreFichier.js**
- Ce code permet de créer, lire, mettre à jour et supprimer des article Strapi avec Axios, en utilisant une boucle pour envoyer un nombre spécifié de requêtes.

- Une fois le code en marche, vous pouvez commencer à remarquer des courbes qui se dessinent dans Grafana (courbe et graphe à paramétrer avant)
